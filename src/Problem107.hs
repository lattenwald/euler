import Control.Applicative
import qualified Data.Graph.Inductive as G
import Data.Graph.Inductive.PatriciaTree
import qualified Data.Set as S
import Data.List
import Data.Char
import Data.Function (on)

-- filename = "107test.txt"
filename = "res/107.txt"

readInt :: String -> Int
readInt = read

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn _ [] = []
splitOn a str = b : splitOn a c
  where (b, c') = break (== a) str
        c = if null c' then c' else tail c'

strToEdges :: Int -> String -> [G.LEdge Int]
strToEdges s = map (\(a, b) -> (s, a, read b)) . filter (all isDigit . snd) . zip [1..] . splitOn ','

-- inputToGraph _ = G.empty
inputToGraph input = G.undir $ G.mkGraph v e
  where
    l = lines input
    v = zipWith (\a _ -> (a, a)) [1..] l
    e = concat $ zipWith strToEdges [1..] l

sortEdges :: Ord a => [G.LEdge a] -> [G.LEdge a]
sortEdges = sortBy (compare `on` third3)

prim g | G.isEmpty g = G.empty
prim g = go t edges g'
  where
    (c, g') = G.matchAny g
    edges = sortEdges $ G.out' c
    t = G.insNode (G.labNode' c) G.empty

first3 (a,_,_) = a
second3 (_,a,_) = a
third3 (_,_,a) = a

gcost :: Gr Int Int -> Int
gcost = sum . map third3 . G.labEdges

go :: Gr Int Int -> [G.LEdge Int] -> Gr Int Int -> Gr Int Int
go t [] _ = t
go t (e:es) g = go t' edges g'
  where
    s = let (_, n, _) = e
        in n
    (Just c, g') = G.match s g
    w = G.labNode' c
    t' = G.insEdge e . G.insNode w $ t
    edges = sortEdges . filter (not . flip G.gelem t' . second3) $ G.out' c ++ es

main = do
  g <-  inputToGraph <$> readFile filename
  let t = prim g
      gcost' :: Gr Int Int -> Int
      gcost' = sum . map third3 . G.labEdges

  -- print $ gcost g `div` 2
  -- print $ gcost t
  -- need to divide by 2 as first gcost counts all edge costs twice
  print $ (gcost g `div` 2) - (gcost t)
