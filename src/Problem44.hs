-- P_n=n(3n−1)/2
-- 1, 5, 12, 22, 35, 51, 70, 92, 117, 145, ...

epsilon :: Double
epsilon = 0.00000000000000000000000001

isPentagonal :: Integral a => a -> Bool
isPentagonal n = abs diff < epsilon
  where diff = i - fromIntegral (round i)
        i = (1 + sqrt (1 + 24 * fromIntegral(n))) / 6

toPentagonal i = (i*(3*i - 1)) `div` 2

ans = f 2
  where
    f i = g i (i-1)
    g i 0 = f (i+1)
    g i k | isPentagonal (n-m) && isPentagonal (n+m) =  (n, m)
      where n = toPentagonal i
            m = toPentagonal k
    g i k = g i (k-1)

main = print (fst ans - snd ans)
