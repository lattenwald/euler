module Euler.Factorial (factorial) where

import Data.MemoTrie

factorial :: (HasTrie a, Integral a) => a -> a
factorial = memo factorial'
  where factorial' 0 = 1
        factorial' n = n * (factorial' (n-1))
