module Euler.Utils ( combinations
                   , splitOn
                   , splitBy
                   , fibs
                   , isSquare
                   , squareRoot
                   , uniq) where

import Data.List (isPrefixOf)
import qualified Data.Set as Set

combinations :: Int -> [a] -> [[a]]
combinations n _ | n <= 0 = error "combinations needs first argument to be positive int!"
combinations _ [] = []
combinations 1 xs = map (:[]) xs
combinations n (x:xs) = [ x:comb | comb <- combinations (n-1) xs] ++ combinations n xs

splitOn :: Eq a => [a] -> [a] -> [[a]]
splitOn _ [] = []
splitOn sep xs = splitOn' sep xs []
  where
    splitOn' sep xs acc | sep `isPrefixOf` xs =
                            acc : splitOn sep (drop (length sep) xs)
    splitOn' sep (x:xs) acc = splitOn' sep xs (acc ++ [x])
    splitOn' _ [] acc = [acc]

splitBy :: Eq a => a -> [a] -> [[a]]
splitBy sep = splitOn [sep]

fibs :: Integral a => [a]
fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

isSquare :: (Integral a) => a -> Bool
isSquare n = (round . sqrt $ fromIntegral n) ^ 2 == n

squareRoot :: Integer -> Integer
squareRoot = floor . sqrt . (fromIntegral :: Integer -> Double)

uniq :: Ord a => [a] -> [a]
uniq = Set.toList . Set.fromList
