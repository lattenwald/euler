module Euler.Types where

data Out = OutI Integer
         | OutS String
         | NoSolution
  deriving Show
