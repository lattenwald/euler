module Euler.Primes ( primes, primeFactors, isPrime
                    , properDivisors ) where

import Euler.Utils
import qualified Data.Set as Set

-- http://www.haskell.org/haskellwiki/Prime_numbers#Postponed_Filters
-- XXX understand that
primes :: Integral a => [a]
primes = 2 : primes'
  where primes' = sieve [3,5..] 9 primes'
        sieve (x:xs) q ps@ ~(p:t)
          | x < q = x : sieve xs q ps
          | True  =     sieve [x | x <- xs, rem x p /= 0] (head t^2) t

-- XXX understand that
primeFactors n | n > 1 = go n primes
  where go n ps@(p:ps')
          | p*p > n = [n]
          | n `rem` p == 0 = p:go (n `quot` p) ps
          | otherwise = go n ps'

properDivisors 1 = [1]
properDivisors n =
  init $ Set.toList $ Set.fromList $
  map product $ allCombinations $ primeFactors n
  where allCombinations xs = [1] : go (length xs) xs
          where go 0 _  = []
                go n xs = combinations n xs ++ go (n-1) xs

-- http://stackoverflow.com/questions/4541415/haskell-prime-test
isPrime :: Integral a => a -> Bool
isPrime 1 = False
isPrime x = null $ filter (\y ->  x `mod` y == 0) $ takeWhile (\y ->  y*y <= x) [2..]
