module Euler.Primes.Totient (totient) where

import Euler.Primes
import Euler.Utils
import Data.MemoTrie

-- http://en.wikipedia.org/wiki/Euler's_totient_function
totient' :: (Integral a, Integral b) => a -> b
totient' n = round $ (fromIntegral n) * product (map (\p -> 1 - 1/(fromIntegral p)) . uniq . primeFactors $ n)

totient :: (HasTrie a, Integral a, Integral b) => a -> b
totient = memo totient'
