-- f k x : split x by numbers no less than k
f k x | k > x = error "wtf"
f k x | k == x = 0
-- f 1 2 = (1 + f 1 1)
-- f 1 5 = (1 + f 1 4) + (1 + f 2 3)
-- f 1 4 = (1 + f 1 3) + (1 + f 2 2)
-- f 1 6 = (1 + f 1 5) + (1 + f 2 4) + (1 + f 3 3)
f k x = sum $ map (\n -> 1 + f n (x - n)) [k .. floor (fromIntegral x / 2)]

answer = f 1 100
main = print answer
