import Data.MemoTrie
import qualified Data.Map as M

-- formulae source
-- http://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Continued_fraction_expansion

m :: Integer -> Int -> Integer
m s 0 = 0
m s n = (d' s (n-1)) * (a' s (n-1)) - m' s (n-1)
m' = memo2 m

d :: Integer -> Int -> Integer
d s 0 = 1
d s n = (s - (m' s n)^2) `div` (d' s (n-1))
d' = memo2 d

a :: Integer -> Int -> Integer
a s 0 = floor . sqrt . fromIntegral $ s
a s n = (a' s 0 + m' s n) `div` (d' s n)
a' = memo2 a

-- tuples version with memoization, to use for period detection
f s | (floor (sqrt $ fromIntegral s)) ^ 2 == s = []
f s = map (\n -> (a' s n, m' s n, d' s n)) [0..]

period :: Ord k => [k] -> Int
period xs = g 0 xs M.empty
  where g _ [] s = M.size s
        g k (x:xs) s =
          case M.lookup x s of
            Just i -> k - i
            Nothing -> g (k+1) xs (M.insert x k s)

g = period . f

ans = length . filter odd . map g $ [1 .. 10000]

main = print ans
