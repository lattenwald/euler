{-# LANGUAGE PartialTypeSignatures #-}

import Euler.Primes.Totient
import Data.List (maximumBy)
import Data.Function (on)

f :: Integer -> Rational
f n = (fromIntegral n) / (fromIntegral . totient $ n)

ans :: Integer
ans = fst . maximumBy (compare `on` snd) . map (\n -> (n, f n)) $ [2..1000000]

main = print ans
