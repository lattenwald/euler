import Data.Char
import Data.List
import Euler.Primes

candidates n = reverse . sort . permutations . map intToDigit $ [1 .. n]

isPrime' = isPrime . read

ans = go 8
  where
    go 0 = error "sorry"
    go n = case (filter isPrime' $ candidates n) of
      [] -> go (n-1)
      (a:_) -> a

main = putStrLn ans
