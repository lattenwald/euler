import Data.List
import Control.Monad

check :: Int -> Int -> Int -> Bool
check a b c = sort (show a ++  show b ++ show c) == "123456789"

a = do
  x <- [1..9]
  y <- [1000..9999]
  let z = x * y
  guard (check x y z)
  return z

b = do
  x <- [10..99]
  y <- [100..999]
  let z = x * y
  guard (check x y z)
  return z

answer = nub $ a ++ b

main = print $ sum answer
