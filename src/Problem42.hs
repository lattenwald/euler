import System.IO
import Data.Char
import Data.List

splitOnC _ [] = []
splitOnC delim str = foldr f [""] str
  where f ch acc = if ch == delim
                   then "" : acc
                   else (ch : head acc) : (tail acc)

stripC c = stripHead . stripTail
  where stripHead [] = []
        stripHead s@(x:xs) | x == c = stripHead xs
                           | otherwise = s
        stripTail [] = []
        stripTail s | last s == c = stripTail (init s)
                    | otherwise = s

inputToList = map (stripC '"') . splitOnC ','

num c = 1 + ord c - ord 'A'

tr n = n * (n+1) `div` 2

sumWord = sum . map num

getAns :: [String] -> Int
getAns ws = foldr f 0 ws
  where
    f w = if isTriangleW w
          then (+1)
          else (+0)
    maxSum = maximum . map sumWord $ ws
    isTriangleW w = (sumWord w) `elem` triangles
    triangles = takeWhile (<= maxSum) . map tr $ [1..]

main = do
  input <- readFile "res/42.txt"
  print $ getAns $ inputToList input
