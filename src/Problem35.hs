{-# LANGUAGE FlexibleContexts #-}

import Euler.Primes
import Data.Array.Unboxed
import Data.Monoid

primesToA m = sieve 3 (array (3,m) [(i,odd i) | i<-[3..m]] :: UArray Int Bool)
  where
    sieve p a
      | p*p > m   = 2 : [i | (i,True) <- assocs a]
      | a!p       = sieve (p+2) $ a//[(i,False) | i <- [p*p, p*p+2*p..m]]
      | otherwise = sieve (p+2) a

minus (x:xs) (y:ys) = case (compare x y) of
  LT -> x : minus  xs  (y:ys)
  EQ ->     minus  xs     ys
  GT ->     minus (x:xs)  ys
minus xs _ = xs

union (x:xs) (y:ys) = case (compare x y) of
  LT -> x : union  xs  (y:ys)
  EQ -> x : union  xs     ys
  GT -> y : union (x:xs)  ys
union xs [] = xs
union [] ys = ys

rotations :: (Read a, Show a) => a -> [a]
rotations n = rot len n
   where len = length $ show n
         rot 0 _ = []
         rot k n = n : rot (k-1) (read $ tail str ++ [head str])
           where
             str = show n

pr = primesToA $ 10^6

a = filter ((all (`elem` pr)) . rotations) pr

-- filtering out numbers containg '0' first, and then checking for primality of rotations
isCircularPrime n =
  '0' `notElem` (show n)
  && (getAll . foldr1 mappend . map (All . isPrime) . rotations) n

l = filter isCircularPrime $ primesToA 1000000

ans = length l

main = print ans

-- main = print $ length a
-- main = print $ primesToA (10^6)
