import Data.Ratio (numerator, denominator)

e :: Integer -> Rational
e 1 = 1 + 1/2
e n = 1 + 1 / (1 + e (n-1))

check k = length (show n) > length (show d)
  where j = e k
        n = numerator j
        d = denominator j

ans = length . filter check $ [1 .. 1000]

main = print ans
