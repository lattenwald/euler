import Data.List
import Euler.Utils

numberedFibs = zip [0..] fibs

answer = fmap fst $ find (\x -> snd x > 10^999) numberedFibs

main = print answer
