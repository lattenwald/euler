import Control.Monad
import Data.Function (on)
import Data.List
import Euler.Utils

m = 1000

l = do
  a <- [1 .. m]
  b <- [a .. m]
  let c2 = a*a + b*b
  guard $ isSquare c2
  let c = squareRoot c2
      p = a + b + c
  guard $ p <= 1000
  return p

a = map (\x -> (x, length . filter (== x) $ l)) [1 .. 1000]

ans = fst $ head $ sortBy (\a b -> (snd b) `compare` (snd a)) a

main = print ans
