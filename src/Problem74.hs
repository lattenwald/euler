import Control.Monad
import Data.List
import Debug.Trace (trace)
import Euler.Factorial


df = sum . map (factorial . read . (:[])) . show

chain = ap (:) $ unfoldr (\i -> let dfi = df i in Just (dfi, dfi))

chainLength [] = 0
chainLength xs = f [] xs
  where f [] (x:xs) = f [x] xs
        f ys (x:xs) | x `notElem` ys = f (x:ys) xs
        f ys (x:_) = length ys

-- length . filter ((60==) . length) .
ans = length . filter (60==) . map (chainLength . chain) $ ([1..1000000] :: [Int])

main = print ans
