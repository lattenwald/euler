import Euler.Primes.Totient

t :: Int -> Int
t = totient

ans = sum  [t n | n <- [2..1000000]]

main = print ans
