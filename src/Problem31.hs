q :: Int -> [Int] -> Int
q 0 _ = 1
q n _ | n < 0 = 0
q _ [] = 0
q n coins@(c:coins') = q n coins' + q (n - c) coins

answer = q 200 [1,2,5,10,20,50,100,200]

main = print answer
