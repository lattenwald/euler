isLychrel :: Integer -> Bool
isLychrel n = check 0 n
  where
    check 50 _ = True
    check i n = not (isPalindrome k) && check (i+1) k
      where k = n + read (reverse $ show n)
            isPalindrome n = show n == reverse (show n)

input = [1 .. 9999]

ans = length . filter isLychrel $ input

main = print ans
