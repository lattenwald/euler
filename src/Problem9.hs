answer = product $ head [ [x,y,z] | x<-a, y<-a, z<-a, x+y+z==1000, z>x, z>y, y>=x, x^2+y^2==z^2]
  where a = [1 .. 1000]

main :: IO ()
main = do
  putStrLn $ show answer
