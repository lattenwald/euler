import Data.List

candidates = reverse . sort . permutations $ "987654321"

-- check x =

pandigital x | sort x /= "123456789" = error "wtf"
pandigital x = check 1 x
  where check n x | n > 6 = False
                  | otherwise = pandigitalWith (read $  take n x) x
                                || check (n+1) x

pandigitalWith :: Integer -> String -> Bool
pandigitalWith n x = f n 1 x
  where
    f :: Integer -> Integer -> String -> Bool
    f _ _ [] = True
    f n i x = isPrefixOf a x && f n (i+1) (drop (length a) x)
      where a = show (n*i)

ans = head . filter pandigital $ candidates

main = putStrLn ans
