import Data.Function
import Data.List

chain 1 = [1]
chain n
  | odd n = n : chain (3 * n + 1)
  | otherwise = n : chain (n `div` 2)

list = map (\x -> (x, length $ chain x)) [1 .. 999999]
answer = (fst . last) $ sortBy (compare `on` snd) list

main = do
  putStrLn $ show answer
