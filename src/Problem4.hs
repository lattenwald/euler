import Data.List

isPalindrome n = fwd == rev
  where fwd = show n
        rev = reverse fwd

palindromes = sort [ x * y | x <- [100 .. 999], y <- [100 .. 999], isPalindrome $ x * y ]

main :: IO ()
main = print $ last palindromes
