import Euler.Primes

answer = sum $ takeWhile (<2000000) primes

main :: IO ()
main = do
  putStrLn $ show answer
