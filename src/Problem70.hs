import Data.List (sort, minimumBy)
import Euler.Primes.Totient
import Data.Function (on)

check n = let s = show n
              s' = show (totient n)
          in length s' == length s && sort s == sort s'

f n = (fromIntegral n) / (fromIntegral . totient $ n)

ans :: Integer
ans = fst $ minimumBy (compare `on` snd) [ (n, f n) | n <- [2..10^7], check n ]

main = print ans
