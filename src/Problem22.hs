import Control.Applicative
import Control.Monad
import Data.List

import Euler.Utils (splitOn)

charScores = zip ['A' .. 'Z'] [1..]

nameScore name = extract $ sum <$> (sequence $ map charScore name)
  where charScore c = lookup c charScores
        extract (Just x) = x

input = "\"EDMUNDO\",\"SID\",\"PORTER\",\"LEIF\""

getAnswer s =
  sum $
  map (\(a,b) -> a*b) $
  zip [1..] $
  map nameScore (sort . (splitOn "," . filter (/= '"')) $ s)

main = fmap getAnswer (readFile "res/22.txt") >>= print
