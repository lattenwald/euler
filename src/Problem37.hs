import Euler.Primes
import Data.Functor
import Data.List
import Debug.Trace (trace)
import Data.Char
import Control.Monad

-- ns = [1,3,7,9]
ns = [1 .. 9]
l = map show ns

f :: MonadPlus m => String -> String -> m String
f a b@[_] | isPrime (read a) && isPrime (read c) = return c
          | otherwise = mzero
  where c = a ++ b
f _ _ = error "wtf"

check :: String -> Bool
check x = checkInit x && checkTail x

checkTail :: String -> Bool
checkTail [] = True
checkTail x@(_:xs) = isPrime (read x) && checkTail xs

checkInit :: String -> Bool
checkInit [] = True
checkInit x = isPrime (read x) && checkInit (init x)

g :: MonadPlus m => String -> m String
-- g a = trace a $ go a l
g a = go a l
  where go a [] = mzero
        go a (x:xs) = (f a x) `mplus` (go a xs)

u :: (Eq (m String), MonadPlus m) => String -> m String
u a = if g' == mzero
      then (return a) `mplus` mzero
      else g' >>= u
  where g' = g a

v :: MonadPlus m => String -> m String
v [] = mzero
v a | checkTail a = return a
    | otherwise = v (init a)

ans1 = filter checkTail $ l >>= u
ans2 = filter checkTail $ map show [2 .. 9] >>= u

sum' = sum . map read

-- main = print . sum . map read $ ans

-- shitloads of crap above, need to do it SOMEDAY, but quickndirty solution below just works.
-- we know that total number of such numbers is 11, and first 4 are going to be 2,3,5,7. Hence
-- drop 4 $ take 15
main = print $ sum' $ drop 4 $ take 15 $ filter check $ map show primes
