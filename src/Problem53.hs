factorial 0 = 1
factorial 1 = 1
factorial n = n * factorial (n - 1)

choose n r = factorial n `div` (factorial r * factorial (n - r))

border = 1000000

check n r = choose n r > border

ans = length [ (n, r) | n <- [2 .. 100], r <- [1 .. (n-1) ], check n r ]

main = print ans
