import Data.Char (digitToInt)

l = concat . map show $ [0..]

ans = product . map (\x -> digitToInt $ l!!x) $ [1,10,100,1000,10000,100000,1000000]

main = print ans
