import Euler.Primes

pdSum = map (\x -> sum $ properDivisors x) [0..]

amicableUnder n = filter isAmicable  [1..(fromIntegral n)]
  where isAmicable x = let pds = pdSum!!x
          in pds < n && pds /= (fromIntegral x) && x == fromIntegral (pdSum!!(fromIntegral pds))

main = print $ sum $ amicableUnder 10000
