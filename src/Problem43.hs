import Control.Applicative
import Data.Monoid
import Data.List
import Data.Char
import Euler.Primes

slice from to = take (to - from + 1) . drop from

nums s = map (\x -> read [s!!(x-1), s!!x, s!!(x+1)]) [2 ..8]

check :: String -> Bool
check s = foldr1 (\acc x -> acc && x) $ map (== 0) list
  where list = getZipList $ mod <$> ZipList (nums s) <*> ZipList primes

candidates = permutations . map intToDigit $ [0 .. 9]

ans = sum . map read . filter check $ candidates

main = print ans
