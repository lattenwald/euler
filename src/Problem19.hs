import Data.Monoid (mconcat)

data Month = Jan | Feb | Mar | Apr | May | Jun | Jul | Aug | Sep | Oct | Nov | Dec
  deriving (Show, Read, Ord, Eq, Bounded, Enum)

data Weekday = Mon | Tue | Wed | Thi | Fri | Sat | Sun
  deriving (Show, Read, Ord, Eq, Bounded, Enum)

isLeap year
  | year `mod` 400 == 0 = True
  | year `mod` 100 == 0 = False
  | year `mod` 4   == 0 = True
  | otherwise           = False

monthLen year mon
  | mon `elem` [Sep, Apr, Jun, Nov] = 30
  | mon == Feb = if isLeap year then 29 else 28
  | otherwise = 31

weekdays = mconcat $ repeat [Mon .. Sun]

dates year = mconcat $ map (\mon -> [1 .. (monthLen year mon)]) [Jan .. Dec]

lastWday year = snd $ last $ zip (mconcat $ map dates [1900 .. year]) weekdays

datesFor firstYear lastYear = zip (mconcat $ map dates [firstYear .. lastYear]) okWdays
  where okWdays = dropWhile (<= lWday) weekdays
        lWday = lastWday $ firstYear - 1

answer = length $ filter (== (1, Sun)) (datesFor 1901 2000)

main = print answer
