import Data.List (foldl1')
import Data.Ratio

ans =
  numerator $
  foldl1' (\acc item -> if item > acc && item < 3%7 then item else acc)
  [ n % d | d <- [999000..1000000], n <- [floor(d * 3 % 7) .. ceiling(d * 3 % 7)] ]

main = print ans
