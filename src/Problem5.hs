import Data.Char
import Data.List

primeFactors n | n > 1 = go n primes
  where go n ps@(p:ps')
          | p*p > n = [n]
          | n `rem` p == 0 = p:go (n `quot` p) ps
          | otherwise = go n ps'

largestPrime n = last $ primeFactors n

primes = filter isPrime [2..]

isPrime :: Integer -> Bool
isPrime x
  | x <= 1 = error "Number should be greater than 0!"
isPrime 2 = True
isPrime 5 = True
isPrime n
  | lastDigit `mod` 2 == 0 = False
  | lastDigit `mod` 5 == 0 = False
  | otherwise = not $ hasFactor (n `div` 2 + 1) n
    where hasFactor x y
            | x == 1 = False
            | y `mod` x == 0 = True
            | otherwise = hasFactor (x-1) y
          lastDigit = digitToInt $ last $ show n

commonPrimes [] = []
commonPrimes (x:xs) = (commonPrimes xs) ++ (primeFactors x \\ commonPrimes xs)

leastCommonMultiple xs = product $ commonPrimes xs

answer = leastCommonMultiple [2..20]

main :: IO ()
main = print answer
