import Control.Applicative
import Data.List (sort)

f n = (n *) <$> [6,5,4,3,2]

check n = all isPermutation $ f n
  where
    s = sort . show $ n
    isPermutation = (s ==) .  sort . show

ans = head . filter check $ [1..]

main = print ans
