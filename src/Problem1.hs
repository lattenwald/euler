isOk x = (x `mod` 3 == 0) || (x `mod` 5 == 0)
calc x = sum $ filter (\x -> isOk x) [1 .. x]

main :: IO ()
main = print $ calc 999
