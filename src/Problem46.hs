import Euler.Primes
import Control.Applicative
import Data.List

n = 5000
l1 = sort $
     filter (\x -> (not . isPrime $ x) && odd x) $
     (+) <$> (take n primes) <*> (take n $ map (\x -> 2 * x^2) [1..])
l2 = filter (not . isPrime) . map (\x -> 1+2*x) $ [1..]

ans = head $ filter (`notElem` l1) l2

main = print ans
