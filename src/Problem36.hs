import Numeric
import Data.Char

check n = ns == reverse ns
          && bs == reverse bs
  where ns = show n
        bs = showIntAtBase 2 intToDigit n ""

answer = sum . filter check $ [1 .. 1000000]

main = print answer
