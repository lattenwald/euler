import Data.Char

check x = x == (foldl (\acc x -> acc + x^5) 0 $ map digitToInt $ show x)
ans = sum [ x | x <- [2..999999], check x ]
main = print ans
