seriesLength = 5
seed = (1, 2, 0)

curNum (n, _, _) = n
curStep (_, s, _) = s
curCycle (_, _, c) = c

chain = go seed
  where go seed@(n, s, c)
          | c == 4 = go (n, s+2, 0)
          | otherwise = seed : go(n+s, s, c+1)

answer = sum $ map curNum $ takeWhile (\(n,s,c) -> s <= 1001 || c == 0 ) chain

main = print answer
