import Control.Monad
import Data.List (sortBy)
import Data.Function (on)
import Euler.Primes

import Debug.Trace (trace)

limit = 1000000

pr = takeWhile (<limit) primes

-- lists _ _ _ [] = []

lists = f 0 0
  where
    prlen = length pr
    f i j | i == j && i == prlen - 1 = []
    f i j = let l = take (j - i + 1) . drop i $ pr
                s = sum l
            in if s >= limit
               then f (i+1) (i+1)
               else if (s `elem` pr)
                    then
                      -- trace (show s ++ " : " ++ show i ++ " to " ++ show j) $
                      (s, length l) : f i (j+1)
                    else f i (j+1)

ans = last . sortBy (compare `on` snd) $ lists

main = print ans

-- oldans = fst . head . sortBy (\a b -> (snd b) `compare` (snd a)) $ do
--   let m = length pr - 1
--   a <- [0 .. m]
--   b <- [a .. m]
--   let s = sum . take (b - a + 1) . drop a $ pr
--   guard $ s < limit
--   guard $ s `elem` pr
--   return (s, b-a)

-- main = print oldans
