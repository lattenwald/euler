import qualified Data.Set as S
main = print $ length $ S.toList $ S.fromList [ a^b | a <- [2..100], b <- [2..100] ]
