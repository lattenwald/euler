import System.IO
import Euler.Primes
import Control.Monad (guard)
-- import Debug.Trace (trace)
trace _ = id

pr :: [Int]
pr = takeWhile (< 25000) primes

l = length pr - 1

check a b = isPrime (read $ show a ++ show b)
             && isPrime (read $ show b ++ show a)

list :: [[Int]]
list = do
  i1 <- [1 .. l]
  let n1 = pr !! i1
  i2 <- [i1+1 .. l]
  let n2 = pr !! i2
  guard $ check n1 n2
  i3 <- [i2+1 .. l]
  let n3 = pr !! i3
  guard $ checkStuff n3 [n1, n2]
  i4 <- [i3+1 .. l]
  let n4 = pr !! i4
  guard $ checkStuff n4 [n1, n2, n3]
  -- return [n1, n2, n3, n4]
  i5 <- [i4+1 .. l]
  let n5 = pr !! i5
  guard $ checkStuff n5 [n1, n2, n3, n4]
  return [n1, n2, n3, n4, n5]


checkStuff n= all (check n)

main = do
  hSetBuffering stdout NoBuffering
  print $ minimum . map sum . take 2 $ list
