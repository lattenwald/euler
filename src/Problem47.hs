import Euler.Primes
import Data.List (group)

primeFactorsCount = length . group . primeFactors

check k n = go (k-1) n
  where go (-1) _ = True
        go i n = (fromIntegral (primeFactorsCount (n+i)) == k) && go (i-1) n

ans = head . filter (check 4) $ [2..]
main = print ans
