import Euler.Primes
import Data.List (sortBy, sort, group, permutations, nub, elemIndices)
import Data.Char (intToDigit)

pr = filter f primes
  where
    f = (== 3) . length . head
        . sortBy (\a b -> (length b) `compare` (length a))
        . group . sort . show

d = head . head . sortBy (\a b -> (length b) `compare` (length a))
    . group . sort . show

p c n = elemIndices c (show n)

p' n = p (d n) n

pr' = map (\n -> (n, p' n)) pr

readInt :: String -> Int
readInt "" = error "wtf"
readInt a = read a

replace p c n = readInt . replace' p c $ (show n)

replace' [] _ xs = xs
replace' (p:ps) x xs = take p xs ++ [x] ++ replace' (map (\a -> a-p-1) ps) x (drop (p+1) xs)

f n = filter isPrime . map (\c -> replace positions (intToDigit c) n) $ digits
  where
    positions = p' n
    digits =  if head positions == 0
              then [1 .. 9]
              else [0 .. 9]

ans = head $ filter ((8==) . length . f) primes

main = print ans
