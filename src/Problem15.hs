-- failed myself, algorythm at http://blog.functionalfun.net/2008/07/project-euler-problem-15-city-grids-and.html

n = 20

-- slow {{{
pascal 0 _ = 1
pascal _ 0 = 1
pascal x y = pascal (x-1) y + pascal x (y-1)
-- }}}

-- fast {{{
pascal' 0 _ = 1
pascal' _ 0 = 1
pascal' x y = pascals'!!(x-1)!!y + pascals'!!x!!(y-1)

pascals' = [[ pascal' x y | y <- [0..]] | x <- [0..]]
-- }}}

main = do
  print $ pascal' n n
