import Data.Ratio

es = (2:) . concat . map (\x -> [1,x*2,1]) $ [1..]

f [x] = x
f (x:xs) = x + 1 / (f xs)

ans n = sum . map (read . (:[])) . show . numerator . f . take n $ es

main = print $ ans 100
