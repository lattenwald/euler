import Data.MemoTrie
import Data.Function (on)
import Data.List (maximumBy)
import Data.Ratio
-- import Debug.Trace (trace)
trace _ = id

-- shamelessly copied from problem 64
m :: Integer -> Int -> Integer
m s 0 = 0
m s n = (d' s (n-1)) * (a' s (n-1)) - m' s (n-1)
m' = memo2 m

d :: Integer -> Int -> Integer
d s 0 = 1
d s n = (s - (m' s n)^2) `div` (d' s (n-1))
d' = memo2 d

a :: Integer -> Int -> Integer
a s 0 = floor . sqrt . fromIntegral $ s
a s n = (a' s 0 + m' s n) `div` (d' s n)
a' = memo2 a

n :: Integer -> Int -> Integer
n _ (-1) = 1
n s 0 = a' s 0
n s i = (a' s i)*(n' s (i-1)) + n' s (i-2)
n' = memo2 n

l :: Integer -> Int -> Integer
l _ (-1) = 0
l _ 0 = 1
l s i = (a' s i)*(l' s (i-1)) + l' s (i-2)
l' = memo2 l

dio :: Integer -> Maybe Integer
dio s | (floor (sqrt $ fromIntegral s)) ^ 2 == s = Nothing
dio s = let check x = (numerator x)^2 - s*(denominator x)^2 == 1
            solutions = map (\i -> let x = (n' s i) % (l' s i)
                                   in trace (show x) x ) $ [1..]
        in Just . numerator . head . filter check $ solutions

ans = fst . maximumBy (compare `on` snd) . takeWhile ((1000>=) . fst) . map (\i -> trace (show i) (i, dio i)) $ [2..]

main = print ans
