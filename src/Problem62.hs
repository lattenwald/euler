import Data.List (groupBy, sort, sortBy)
import Data.Function (on)
import Data.Maybe (isJust, fromJust)
import Data.Monoid

cubes = map (show . (^3)) [1..]
cubes' = groupBy ((==) `on` length) cubes

check n xs = case filter ((n==) . fst)
                  . map (\(a@((_,x):_)) -> (length a, x))
                  . groupBy ((==) `on` fst)
                  . sortBy (compare `on`  fst)
                  . map (\a -> (sort a, a)) $ xs
             of ((_,y):_) -> Just y
                [] -> Nothing

ans n = fromJust . head . filter isJust . map (check n) $ cubes'

ans' n = fromJust . getFirst . mconcat . map (First . check n) $ cubes'

main = putStrLn (ans' 5)
