import Data.Function (on)
import Data.List (permutations, minimumBy, elemIndex, sort)
-- import Debug.Trace (trace)
trace _ = id

p1 :: [Int] -> [[Int]] -> [[Int]]
p1 [a] acc        = [a, head . last $ acc] : acc
p1 (a:b:rest) acc = p1 (b:rest) ([a,b]:acc)

ring :: [Int] -> [[Int]]
ring xs = let l = length xs
              l2 = l `div` 2
              p = reverse $ p1 (drop (l2) xs) []
          in trace (show p) $ zipWith (:) (take l2 xs) p

isMagic :: [[Int]] -> Bool
isMagic ring = let s = sum . head $ ring
               in all ((s ==) . sum) (tail ring)

magicRings :: [Int] -> [[[Int]]]
magicRings = filter isMagic . map ring . permutations

rotate :: Int -> [[Int]] -> [[Int]]
rotate _ [] = []
rotate 0 xs = xs
rotate n (x:xs) = rotate (n-1) (xs ++ [x])

canonRing :: [[Int]] -> [[Int]]
canonRing ring = rotate im ring
  where m = minimumBy (compare `on` head) ring
        Just im = elemIndex m ring

ringToString :: [[Int]] -> String
ringToString = concat . concat . map (map show) . canonRing

ans :: String
ans = head . reverse . sort . filter ((16==) . length) . map ringToString . magicRings $ [1..10]

main = putStrLn ans
