import System.IO

inputToList input = map (\x -> map (\y -> read y) $ words x) $ lines input
-- saw that at http://projecteuler.net/thread=18&page=7, pretty impressive
getAnswer = foldr1 (\x y -> zipWith (+) x (zipWith max y (tail y)))

main = do
  dataList <- readFile "res/67.txt"
  print $ getAnswer $ inputToList dataList
