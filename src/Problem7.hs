import Euler.Primes

answer = primes !! 10000

main = do
  putStrLn $ show answer
