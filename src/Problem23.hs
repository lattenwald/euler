import qualified Data.Set as S
import Control.Applicative
import Euler.Primes (properDivisors)

upperLimit = 28123

isAbundant x = x < sum (properDivisors x)

abundants = filter isAbundant [1..]

ourAbundants = takeWhile (<upperLimit) abundants

okays = S.toList $ S.fromList $ filter (<=upperLimit) $ (+) <$> ourAbundants <*> ourAbundants

answer = sum $ filter (`notElem` okays) [1 .. upperLimit]

main = print answer
