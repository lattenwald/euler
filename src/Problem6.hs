answer = sum [ if x /= y then x * y else 0 | x <- [1..100], y <- [1..100]]

main :: IO ()
main = print answer
