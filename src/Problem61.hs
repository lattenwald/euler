import Data.List (permutations)
import Control.Monad

tw = dropWhile (<=10^3) . takeWhile (< 10 ^ 4)

triangles = tw $ map (\n -> n * (n+1) `div` 2) [1..]
squares = tw $ map (^2) [1..]
pentagonals = tw $ map (\n -> n*(3*n-1) `div` 2) [1..]
hexagonals = tw $ map (\n -> n*(2*n-1)) [1..]
heptagonals = tw $ map (\n -> n*(5*n-3) `div` 2) [1..]
octagonals = tw $ map (\n -> n*(3*n-2)) [1..]

check2 a b = a `mod` 100 == b `div` 100

test a stuff = do
  let [b', c'] = stuff
  b <- filter (check2 a) b'
  c <- filter (check2 b) c'
  guard (check2 c a)
  [a,b,c]

test' = map (test 8128) $ permutations [squares, pentagonals]

test'' = filter (not . null) $ do
  a <- triangles
  let r = map (test a) $ permutations [squares, pentagonals]
  r

res a stuff = do
  let [b', c', d', e', f'] = stuff
  b <- filter (check2 a) b'
  c <- filter (check2 b) c'
  d <- filter (check2 c) d'
  e <- filter (check2 d) e'
  f <- filter (check2 e) f'
  guard (check2 f a)
  [a,b,c,d,e,f]

ans = filter (not . null) $ do
  a <- triangles
  let r = map (res a) $ permutations [squares, pentagonals, hexagonals, heptagonals, octagonals]
  r

main = print $ sum . head $ ans
