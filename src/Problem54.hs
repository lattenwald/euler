import System.IO
import Data.Char (digitToInt)
import Data.List (sort, nub, splitAt, partition, intercalate)
import Control.Monad
import Control.Monad.Writer

type Log = Writer [String]

data Card = Card Int Char
          deriving (Show, Eq, Ord)

data Hand = HighCard Int | Pair Int | TwoPairs [Int] | Three Int | Straight Int | Flush [Int] | FullHouse (Int, Int) | Four Int | StraightFlush Int | RoyalFlush
          deriving (Show, Eq, Ord)

parseHands' :: String -> Log ([Card], [Card])
parseHands' s = do
  tell [s1, s2]
  return $ mapPair (reverse . sort) $ (h1, h2)
  where (s1, s2) = fmap tail . splitAt 14 $ s
        h1 = map readCard . words $ s1
        h2 = map readCard . words $ s2

evaluateHand' :: [Card] -> Log (Hand, [Card])
evaluateHand' hand = do
  tell [show h]
  return (h, c)
  where c = reverse . sort $ hand
        h = head . reverse . sort . nub . map look $ combinations c

check' :: String -> Log Bool
check' s = do
  (h1, h2) <- parseHands' s
  e1 <- evaluateHand' h1
  e2 <- evaluateHand' h2
  let fwins = compareHands (e1, e2) == GT
  tell [(if fwins then "First"
         else "Second") ++ " player wins"]
  tell ["\n"]
  return fwins

readCard :: String -> Card
readCard str = Card val suit
  where (valChar:suit:[]) = str
        val = case valChar of
          'T' -> 10
          'J' -> 11
          'Q' -> 12
          'K' -> 13
          'A' -> 14
          otherwise -> digitToInt valChar

suit :: Card -> Char
suit (Card _ c) = c

value :: Card -> Int
value (Card v _) = v

sameSuit [_] = True
sameSuit (a:b:rest) = suit a == suit b && sameSuit (b:rest)
isAce c = value c == 13

consecutive [_] = True
consecutive (a:b:rest) = value a == value b + 1 && consecutive (b:rest)

consecutive' :: [Card] -> Log Bool
consecutive' [_] = do
  tell ["Finished."]
  return True
consecutive' (a:b:rest) = do
  tell [show (value a) ++ " vs " ++ show (value b)]
  c <- consecutive' (b:rest)
  let c' = value a - 1 == value b
  tell [if c' then "Ok" else "Fail"]
  return $ if value a - 1 == value b
           then c
           else False

isFull hand = length hand == 5

look hand@(a:_) | isFull hand && sameSuit hand && consecutive hand && isAce a = RoyalFlush
look hand@(a:_) | isFull hand && sameSuit hand && consecutive hand = StraightFlush $ value a
look hand@(a:b:c:d:_) | value a == value b && value a == value c && value a == value d = Four $ value a
look hand@(a:b:c:d:e:_) | isFull hand && value a == value b && value c == value d && value c == value e = FullHouse (value c, value a)
look hand@(a:b:c:d:e:_) | isFull hand && value a == value b && value a == value c && value d == value e = FullHouse (value a, value d)
look hand | isFull hand && sameSuit hand = Flush $ map value hand
look hand@(a:_) | isFull hand && consecutive hand = Straight $ value a
look hand@(a:b:c:_) | value a == value b && value a == value c = Three $ value a
look hand@(a:b:c:d:_) | value a == value b && value c == value d = TwoPairs [value a, value c]
look hand@(a:b:_) | value a == value b = Pair $ value a
look hand = HighCard . value . head. reverse. sort $ hand

mapPair f (a, b) = (f a, f b)

compareHands ((a, a'), (b, b')) = if cmpa == EQ
                                  then cmpb
                                  else cmpa
  where cmpa = a `compare` b
        cmpb = a' `compare` b'

combinations :: [a] -> [[a]]
combinations a = concat (map (\n -> combinations' n a) [1 .. length a])
  where
    combinations' 0 _ = [[]]
    combinations' _ [] = []
    combinations' n (x:xs) = (map (x:) (combinations' (n-1) xs)) ++ (combinations' n xs)

filename = "res/54.txt"

main = do
  input <- readFile filename
  let (a, log) = runWriter $ mapM check' $ lines input
      ans = length . filter id $ a
  putStr $ intercalate "\n" log
  putStrLn $ "Answer is " ++ show ans

test = do
  (a, b) <- parseHands' "5A 4A 3A 2A 1A AA KA QA JA TA"
  c1 <- consecutive' a
  c2 <- consecutive' b
  return (c1, c2)
