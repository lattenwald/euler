-- vim:fdm=marker
import Data.Char
import System.IO
import Euler.Utils

-- working {{{
inputToList :: String -> [[Integer]]
inputToList input = map (\x -> map read $ splitOn "," x) $ lines input

row n xs m = go n xs
  where go 0 xs
          | n >= len = [m]
          | otherwise = [xs!!0!!n]
        go i xs
          | i >= len = m : goTail
          | (n-i) >= len = m : goTail
          | otherwise = xs!!i!!(n-i) : goTail
            where goTail = go (i-1) xs
        len = length xs

toTriangle xs = reverse $ go (2*(len-1)) xs
  where go (-1) _ = []
        go n xs = row n xs m : go (n-1) xs
        len = length xs
        m = maximum . concat $ xs

getTriAnswer = foldr1 (\a b -> zipWith (\x y -> min x y) (zipWith (+) a b) (zipWith (+) a (tail b)))

main = do
  input <- readFile "res/81.txt"
  print $ getTriAnswer $ toTriangle $ inputToList input

-- }}}

-- {{{ testing and solving process, шитloads o'crap

input =
  "131,673,234,103,18\n\
 \ 201,96,342,965,150\n\
 \ 630,803,746,422,111\n\
 \ 537,699,497,121,956\n\
 \ 805,732,524,37,331"

list :: [[Integer]]
list = inputToList input

triList = toTriangle list
rhombList = toRhomb list

rowR n xs = go n xs
  where go 0 xs
          | n >= len = []
          | otherwise = [xs!!0!!n]
        go i xs
          | i >= len = goTail
          | (n-i) >= len = goTail
          | otherwise = xs!!i!!(n-i) : goTail
            where goTail = go (i-1) xs
        len = length xs

toRhomb xs = go (2*(len-1)) xs
  where go (-1) _ = []
        go n xs = rowR n xs : go (n-1) xs
        len = length xs


secondList a b
  | length a > length b = zipWith (+) (tail a) b
  | otherwise = zipWith (+) (head a:a) b

firstList a b
  | length a < length b = zipWith (+) a b
  | otherwise = zipWith (+) a (head b:b)

--getTriAnswer = scanr1 (\a b -> zipWith (\x y -> min x y) (zipWith (+) a b) (zipWith (+) a (tail b)))

getRhombAnswer = scanr1 (\a b -> zipWith (\x y -> min x y) (firstList a b) (secondList a b))
-- }}}



--http://projecteuler.net/thread=81;page=8
--module Problem0081 where
--
--reduce a [] = scanr1 (+) a
--reduce [a] [b] = [a + b]
--reduce (a:as) (b:bs) = a + (min b (head r)) : r
--    where r = reduce as bs
--
--main = do
--    file <- readFile "matrix.txt"
--    let m = map (\line -> read ("[" ++ line ++ "]")) (lines file) :: [[Int]]
--    print $ head $ foldr reduce [] m
