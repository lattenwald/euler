import           Euler.Types
import           System.Environment
import qualified Problem1
import qualified Problem2
import qualified Problem3
import qualified Problem4
import qualified Problem5
import qualified Problem6

runProblem :: String -> Out
runProblem "1" = Problem1.run
runProblem "2" = Problem2.run
runProblem "3" = Problem3.run
runProblem "4" = Problem4.run
runProblem "5" = Problem5.run
runProblem "6" = Problem6.run
runProblem _ = NoSolution

main :: IO ()
main = runProblem . head <$> getArgs >>= print

