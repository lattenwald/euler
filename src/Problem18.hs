import Data.List
import Data.Function
import Prelude hiding (traverse)

input = "75 \n\
 \ 95 64 \n\
 \ 17 47 82 \n\
 \ 18 35 87 10 \n\
 \ 20 04 82 47 65 \n\
 \ 19 01 23 75 03 34 \n\
 \ 88 02 77 73 07 63 67 \n\
 \ 99 65 04 28 06 16 70 92 \n\
 \ 41 41 26 56 83 40 80 70 33 \n\
 \ 41 48 72 33 47 32 37 16 94 29 \n\
 \ 53 71 44 65 25 43 91 52 97 51 14 \n\
 \ 70 11 33 28 77 73 17 78 39 68 17 57 \n\
 \ 91 71 52 38 17 14 91 43 58 50 27 29 48 \n\
 \ 63 66 04 68 89 53 67 30 73 16 69 87 40 31 \n\
 \ 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"

dataList = map (\x -> map (\y -> read y) $ words x) $ lines input

dataMap = map dataListToMaps dataList
  where dataListToMaps xs = zip [startIndex, (startIndex + 2) ..] xs
         where len = length xs
               startIndex = [1, 0..] !! len

traverse 0 = dataMap !! 0
traverse n =
  zipWith zipMax zippedLeft zippedRight
  where shiftedLeft = ( map shiftLeft ( traverse (n-1) ) ) ++ [(n, 0)]
        shiftedRight = ((-n), 0) : map shiftRight ( traverse (n-1) )
        shiftLeft (x, y) = (x - 1, y)
        shiftRight (x, y) = (x + 1, y)
        zippedLeft = zipWith zipSum shiftedLeft $ dataMap !! n
        zippedRight = zipWith zipSum shiftedRight $ dataMap !! n
        zipSum x y = (fst x, snd x + snd y)
        zipMax x y = (fst x, if snd x > snd y then snd x else snd y)


answer = snd $ maximumBy (compare `on` snd) $ traverse $ length dataMap - 1

main = print answer
