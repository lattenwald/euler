lowerBoundary n = let n' = fromIntegral n
                  in round $ 0.49 + 10 ** ((n' - 1) / n')

upperBoundary = 10

ans =
  sum
  . map (\(n, l) -> length . filter ((n==) . length . show) . map (^n) $ [l..10])
  . zip [1..]
  . takeWhile (<10)
  . map lowerBoundary $ [1..]

main = print ans
