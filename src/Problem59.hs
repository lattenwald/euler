import System.IO
import Data.Char
import Euler.Utils
import Data.List (splitAt, transpose)
import Data.Bits (xor)
import Control.Applicative

groupByN :: Int -> [a] -> [[a]]
groupByN _ [] = []
groupByN n xs = a : groupByN n b
  where (a, b) = splitAt n xs

inputToData :: String -> [Int]
inputToData = map read . splitBy ','

filename = "res/59.txt";

letters = map ord ['a' .. 'z']

pwlen = 3

decrypt :: [Int] -> [Int] -> [Int]
decrypt pwd ct = zipWith xor ct (concat $ repeat pwd)

intArrayToStr = map chr

passwordCandidate :: [Int] -> [Int]
passwordCandidate arr = filter (flip pwdIsGood arr) letters

passwords :: [[Int]] -> [[Int]]
passwords d = let d' = transpose d
                  p = map passwordCandidate d'
                  (p1:p2:p3:[]) = p
              in [ [a,b,c] | a <- p1, b <- p2, c <- p3 ]

isGood :: Int -> Bool
isGood c = isAscii c' && (isAlphaNum c' || isSpace c' || isPunctuation c')
  where c' = chr c

pwdIsGood :: Int -> [Int] -> Bool
pwdIsGood _ [] = True
pwdIsGood a (x:xs) = isGood (a `xor` x) && pwdIsGood a xs

main = do
  input <- inputToData <$> readFile filename
  let d = groupByN 3 input
      p = passwords d
      pt = decrypt (head p) input
      ans = sum pt
  putStrLn $ "Password: " ++ show p
  putStrLn $ "Text: " ++ intArrayToStr pt
  putStrLn $ "Answer: " ++ show ans

