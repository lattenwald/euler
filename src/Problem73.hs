import Euler.Utils
import Data.Ratio

ans = length . uniq $ [ frac | d <- [2..12000], n <- [ceiling(d % 3)..floor(d % 2)], let frac = n % d, frac > 1 % 3, frac < 1 % 2 ]

main = print ans
