-- P_n=n(3n−1)/2
-- 1, 5, 12, 22, 35, 51, 70, 92, 117, 145, ...

epsilon :: Double
epsilon = 0.00000000000000000000000001

isF :: Integral a => (a -> Double) -> a -> Bool
isF f n = abs diff < epsilon
  where diff = i - fromIntegral (round i)
        i = f n

isPentagonal :: Integral a => a -> Bool
isPentagonal = isF $ \n -> (1 + sqrt (1 + 24 * fromIntegral n)) / 6

isTriangular :: Integral a => a -> Bool
isTriangular = isF $ \n -> (-1 + sqrt(1 + 8 * fromIntegral n)) / 2

toHexagonal :: Integral a => a -> a
toHexagonal i = i * (2*i - 1)

ans = f 144
  where f i | isPentagonal n && isTriangular n = n
          where n = toHexagonal i
        f i = f (i+1)

main = print ans
