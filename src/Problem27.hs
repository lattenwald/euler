import Data.Function
import Data.List
import Euler.Primes

checkPrime a b n = isPrime $ n^2 + a*n + b

chainLength a b = length $ takeWhile (checkPrime a b) [0..]

lengths = [(a * b, chainLength a b) | a <- [-999 .. 999], b <- (takeWhile (<1000) primes)]

answer = fst $ head $ sortBy ((flip compare) `on` snd) lengths

main = print answer
