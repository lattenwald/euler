import Data.Char

chain 1 = 1
chain 89 = 89
chain n = chain m
  where m = sum $ map (^2) $ map digitToInt $ show n

main = print $ length $ filter (\x -> chain x == 89) [1 .. 10^7]
