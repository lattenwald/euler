import Data.List
import Data.Function

step 0 d = []
step n d | d > n = (0, 0) : step (n*10) d
step n d = tuple : step (nmodd * 10) d
    where tuple@(_, nmodd) = n `divMod` d

finished d = last $ takeWhile (\(_:x:xs) -> x == (0,0) || x `notElem` xs) $ drop 2 $ map reverse $ inits $ step 1 d

fracLen d = fmap (+1) $ elemIndex x xs
  where (x:xs) = finished d

l = map (\x -> (x, fracLen x)) [2 .. 1000]
answer = fst $ last $ sortBy (compare `on` snd) l

main = print answer
