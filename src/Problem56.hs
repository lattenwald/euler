list = [a^b | a <- [1 .. 99], b <- [1 .. 99]]

list' = map (sum . map (read . (:[])) . show) list

ans = maximum list'

main = print ans
