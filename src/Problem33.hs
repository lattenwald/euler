import Data.List
import Data.Ratio

check :: Int -> Int -> Bool
check a b =
  length c == 1 && c /= "0" && e /= 0 &&
  a % b == d % e
  where c = intersect (show a) (show b)
        d = read $ show a \\ c
        e = read $ show b \\ c

a = [ a % b | a <- [10..99], b <- [a..99], check a b]

answer = foldl1 (*) a

main = print $ denominator answer
