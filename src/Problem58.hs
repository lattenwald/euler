import Euler.Primes

layer 0 = [1]
layer n = map (\i -> l + i*s) [0 .. 3]
  where l = s + last (layer (n-1))
        s = 2*n

-- ratio n = (fromIntegral . length . filter isPrime $ d) / (fromIntegral . length $ d)
--   where d = diagonals n

-- ans = s
--   where s = head . filter (\n -> ratio n < 0.1) $ [1..]

spiral = concat . map layer $ [0..]

primeSpiral = map isPrime spiral

nthLayer n = take (1 + 4*n) primeSpiral

ratio n = (fromIntegral . length . filter id . nthLayer $ n) / (fromIntegral $ 1 + 4*n)

ans = 2 * (head . filter ((<0.1) . ratio) $ [1..]) + 1

main = print ans
