import Data.Char

-- fast memoized factorial
fact' :: (Int -> Int) -> Int -> Int
fact' _ n | n < 0 = error "wtf"
fact' _ 0 = 1
fact' f n = n * f (n-1)

fact_list :: [Int]
fact_list = map (fact' fact) [0..]

fact :: Int -> Int
fact n = fact_list !! n

check :: Int -> Bool
check n = length m > 1 && n == sum (map fact k)
  where m = show n
        k = map digitToInt m

main =  print $ sum $ filter check [0 .. 9999999]
