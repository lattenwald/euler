import Euler.Utils (combinations)
import Euler.Primes (primeFactors)
import Data.List
import qualified Data.Set as Set

allCombinations xs = [1] : go (length xs) xs
  where go 0 _  = []
        go n xs = combinations n xs ++ go (n-1) xs

allFactors n =
  Set.toList $ Set.fromList $
  map product $ allCombinations $ primeFactors n

triangleNums = map (\n -> sum [1..n]) [2..]

result = find (\x -> length (allFactors x) > 500) triangleNums

answer = unMaybe result
  where unMaybe (Just x) = show x
        unMaybe Nothing = "Sorry, we got nothing"

main = do
  putStrLn answer
